import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.DetektCreateBaselineTask

plugins {
    kotlin("jvm") version libs.versions.kotlin
    alias(libs.plugins.ktor)
    alias(libs.plugins.serialization)
    alias(libs.plugins.detekt)
    alias(libs.plugins.versions)
    alias(libs.plugins.exposed)
}

repositories {
    mavenCentral()
}

group = "hr.ech.journal"
version = "0.0.1"
val projectJvmTarget = 21

kotlin {
    jvmToolchain(projectJvmTarget)
}

detekt {
    buildUponDefaultConfig = true // preconfigure defaults
    allRules = false // activate all available (even unstable) rules.
    config.setFrom("$projectDir/config/detekt.yml")
    // point to your custom config defining rules to run, overwriting default behavior
    baseline = file("$projectDir/config/baseline.xml") // a way of suppressing issues before introducing detekt
}

tasks.withType<Detekt>().configureEach {
    reports {
        html.required.set(true) // observe findings in your browser with structure and code snippets
        xml.required.set(true) // checkstyle like format mainly for integrations like Jenkins
        txt.required.set(true)
        // similar to the console output, contains issue signature to manually edit baseline files
        sarif.required.set(true)
        // standardized SARIF format (https://sarifweb.azurewebsites.net/)
        // to support integrations with GitHub Code Scanning
        md.required.set(true) // simple Markdown format
    }
}

tasks.withType<Detekt>().configureEach {
    jvmTarget = projectJvmTarget.toString()
}
tasks.withType<DetektCreateBaselineTask>().configureEach {
    jvmTarget = projectJvmTarget.toString()
}

application {
    mainClass.set("hr.ech.journal.ApplicationLocalKt")

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    mavenCentral()
}

dependencies {
    ktorDependencies()
    implementation(libs.logback.classic)
    implementation(libs.bundles.koin)
    implementation(libs.jedis)
    implementation(libs.passay)
    dbDependencies()
    implementation(libs.bcrypt)
    testImplementation(libs.ktor.server.tests.jvm)
    testImplementation(libs.kotlin.test.junit)
}

fun DependencyHandlerScope.ktorDependencies() {
    implementation(libs.bundles.ktor.server)
    implementation(libs.bundles.ktor.client)
}

fun DependencyHandlerScope.dbDependencies() {
    implementation(libs.bundles.exposed)
    implementation(libs.postgresql)
    implementation(libs.flyway.core)
    implementation(libs.hikari)
    runtimeOnly(libs.flyway.database.postgresql)
}

fun isNonStable(version: String): Boolean {
    val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { version.uppercase().contains(it) }
    val nonStableKeyword = listOf("BETA", "RC").any { version.uppercase().contains(it) }
    val regex = "^[0-9,.v-]+(-r)?$".toRegex()
    val isStable = (stableKeyword || regex.matches(version)) && !nonStableKeyword
    return isStable.not()
}

tasks.withType<DependencyUpdatesTask> {
    rejectVersionIf {
        isNonStable(candidate.version) && !isNonStable(currentVersion)
    }
}
