package changelog

databaseChangeLog {
    changeSet(id: '1', author: 'liquibase') {
        createTable(tableName: 'app_user') {
            column(name: 'id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'display_name', type: 'varchar(64)') {
                constraints(nullable: 'false')
            }
            column(name: 'email', type: 'varchar(128)') {
                constraints(nullable: 'false')
            }
            column(name: 'password_hash', type: 'varchar(72)') {
                constraints(nullable: 'false')
            }
            column(name: 'active', type: 'boolean') {
                constraints(nullable: 'false')
            }
        }
        addPrimaryKey(
                constraintName: 'app_user_pk_1',
                tableName: 'app_user',
                columnNames: 'id',
        )
        addDefaultValue(
                tableName: 'app_user',
                columnName: 'active',
                columnDataType: 'boolean',
                defaultValueBoolean: 'false',
        )
        addUniqueConstraint(
                constraintName: 'app_user_uq_1',
                tableName: 'app_user',
                columnNames: 'email',
        )
        addUniqueConstraint(
                constraintName: 'app_user_uq_2',
                tableName: 'app_user',
                columnNames: 'display_name',
        )

        createTable(tableName: 'environment') {
            column(name: 'id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'name', type: 'varchar(64)') {
                constraints(nullable: 'false')
            }
            column(name: 'description', type: 'varchar(256)') {
                constraints(nullable: 'false')
            }
            column(name: 'created_at', type: 'bigint') {
                constraints(nullable: 'false')
            }
        }
        addPrimaryKey(
                constraintName: 'environment_pk_1',
                tableName: 'environment',
                columnNames: 'id',
        )

        createTable(tableName: 'quest') {
            column(name: 'id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'title', type: 'varchar(64)') {
                constraints(nullable: 'false')
            }
            column(name: 'short_description', type: 'varchar(128)') {
                constraints(nullable: 'false')
            }
            column(name: 'long_description', type: 'text') {
                constraints(nullable: 'false')
            }
            column(name: 'created_at', type: 'bigint') {
                constraints(nullable: 'false')
            }
            column(name: 'expires', type: 'boolean') {
                constraints(nullable: 'false')
            }
            column(name: 'expires_at', type: 'bigint') {
                constraints(nullable: 'true')
            }
            column(name: 'created_by', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'environment_id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
        }
        addPrimaryKey(
                constraintName: 'quest_pk_1',
                tableName: 'quest',
                columnNames: 'id',
        )
        addDefaultValue(
                tableName: 'quest',
                columnName: 'expires',
                columnDataType: 'boolean',
                defaultValueBoolean: 'false',
        )
        addForeignKeyConstraint(
                constraintName: 'quest_fk_1',
                baseTableName: 'quest',
                baseColumnNames: 'created_by',
                referencedTableName: 'app_user',
                referencedColumnNames: 'id',
        )
        addForeignKeyConstraint(
                constraintName: 'quest_fk_2',
                baseTableName: 'quest',
                baseColumnNames: 'environment_id',
                referencedTableName: 'environment',
                referencedColumnNames: 'id',
        )

        createTable(tableName: 'quest_item') {
            column(name: 'id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'quest_id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'title', type: 'varchar(64)') {
                constraints(nullable: 'false')
            }
            column(name: 'short_description', type: 'varchar(128)') {
                constraints(nullable: 'false')
            }
            column(name: 'long_description', type: 'text') {
                constraints(nullable: 'false')
            }
            column(name: 'type', type: 'varchar(32)') {
                constraints(nullable: 'false')
            }
            column(name: 'created_at', type: 'bigint') {
                constraints(nullable: 'false')
            }
            column(name: 'expires', type: 'boolean') {
                constraints(nullable: 'false')
            }
            column(name: 'expires_at', type: 'bigint') {
                constraints(nullable: 'true')
            }
            column(name: 'optional', type: 'boolean') {
                constraints(nullable: 'false')
            }
        }
        addPrimaryKey(
                constraintName: 'quest_item_pk_1',
                tableName: 'quest_item',
                columnNames: 'id',
        )
        addDefaultValue(
                tableName: 'quest_item',
                columnName: 'optional',
                columnDataType: 'boolean',
                defaultValueBoolean: 'false',

        )
        addDefaultValue(
                tableName: 'quest',
                columnName: 'expires',
                columnDataType: 'boolean',
                defaultValueBoolean: 'false',
        )
        addForeignKeyConstraint(
                constraintName: 'quest_item_fk_1',
                baseTableName: 'quest_item',
                baseColumnNames: 'quest_id',
                referencedTableName: 'quest',
                referencedColumnNames: 'id',
        )

        createTable(tableName: 'user_quest') {
            column(name: 'user_id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'quest_id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
        }
        addPrimaryKey(
                constraintName: 'user_quest_pk_1',
                tableName: 'user_quest',
                columnNames: 'user_id, quest_id'
        )
        addUniqueConstraint(
                constraintName: 'user_quest_uq_1',
                tableName: 'user_quest',
                columnNames: 'user_id, quest_id'
        )
        addForeignKeyConstraint(
                constraintName: 'user_quest_fk_1',
                baseTableName: 'user_quest',
                baseColumnNames: 'user_id',
                referencedTableName: 'app_user',
                referencedColumnNames: 'id'
        )
        addForeignKeyConstraint(
                constraintName: 'user_quest_fk_2',
                baseTableName: 'user_quest',
                baseColumnNames: 'quest_id',
                referencedTableName: 'quest',
                referencedColumnNames: 'id'
        )

        createTable(tableName: 'user_quest_item') {
            column(name: 'user_id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'quest_item_id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
        }
        addPrimaryKey(
                constraintName: 'user_quest_item_pk_1',
                tableName: 'user_quest_item',
                columnNames: 'user_id, quest_item_id'
        )
        addUniqueConstraint(
                constraintName: 'user_quest_item_uq_1',
                tableName: 'user_quest_item',
                columnNames: 'user_id, quest_item_id'
        )
        addForeignKeyConstraint(
                constraintName: 'user_quest_item_fk_1',
                baseTableName: 'user_quest_item',
                baseColumnNames: 'user_id',
                referencedTableName: 'app_user',
                referencedColumnNames: 'id'
        )
        addForeignKeyConstraint(
                constraintName: 'user_quest_item_fk_2',
                baseTableName: 'user_quest_item',
                baseColumnNames: 'quest_item_id',
                referencedTableName: 'quest_item',
                referencedColumnNames: 'id'
        )

        createTable(tableName: 'quest_item_status') {
            column(name: 'id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'quest_item_id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'status', type: 'varchar(32)') {
                constraints(nullable: 'false')
            }
            column(name: 'timestamp', type: 'bigint') {
                constraints(nullable: 'false')
            }
        }
        addPrimaryKey(
                constraintName: 'quest_item_status_pk_1',
                tableName: 'quest_item_status',
                columnNames: 'id'
        )
        addForeignKeyConstraint(
                constraintName: 'quest_item_status_fk_1',
                baseTableName: 'quest_item_status',
                baseColumnNames: 'quest_item_id',
                referencedTableName: 'quest_item',
                referencedColumnNames: 'id'
        )

        createTable(tableName: 'environment_user') {
            column(name: 'environment_id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'user_id', type: 'varchar(36)') {
                constraints(nullable: 'false')
            }
            column(name: 'permissions', type: 'json') {
                constraints(nullable: 'false')
            }
        }
        addPrimaryKey(
                constraintName: 'environment_user_pk_1',
                tableName: 'environment_user',
                columnNames: 'environment_id,user_id'
        )
        addForeignKeyConstraint(
                constraintName: 'environment_user_fk_1',
                baseTableName: 'environment_user',
                baseColumnNames: 'environment_id',
                referencedTableName: 'environment',
                referencedColumnNames: 'id'
        )
        addForeignKeyConstraint(
                constraintName: 'environment_user_fk_2',
                baseTableName: 'environment_user',
                baseColumnNames: 'user_id',
                referencedTableName: 'app_user',
                referencedColumnNames: 'id'
        )
    }
}
