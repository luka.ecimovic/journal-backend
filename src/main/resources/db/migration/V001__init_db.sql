CREATE TABLE app_user (
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    display_name varchar(64) NOT NULL,
    email varchar(128) NOT NULL,
    password_hash varchar(72) NOT NULL,
    active boolean NOT NULL DEFAULT false,
    CONSTRAINT app_user_pk_1 PRIMARY KEY(id),
    CONSTRAINT app_user_uq_1 UNIQUE (email),
    CONSTRAINT app_user_uq_2 UNIQUE (display_name)
);

CREATE TABLE environment(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    name varchar(64) NOT NULL,
    description varchar(256) NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    CONSTRAINT environment_pk_1 PRIMARY KEY(id)
);

CREATE TABLE environment_user(
    environment_id uuid NOT NULL,
    user_id uuid NOT NULL,
    CONSTRAINT environment_user_pk_1 PRIMARY KEY (environment_id,user_id),
    CONSTRAINT environment_user_fk_1 FOREIGN KEY (environment_id) REFERENCES environment (id),
    CONSTRAINT environment_user_fk_2 FOREIGN KEY (user_id) REFERENCES app_user (id)
);

CREATE TABLE quest(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    title varchar(64) NOT NULL,
    subtitle varchar(128) NOT NULL,
    description text,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    recurring boolean NOT NULL,
    expires_at timestamptz,
    created_by uuid NOT NULL,
    environment_id uuid NOT NULL,
    CONSTRAINT quest_pk_1 PRIMARY KEY(id),
    CONSTRAINT quest_fk_1 FOREIGN KEY (created_by) REFERENCES app_user (id),
    CONSTRAINT quest_fk_2 FOREIGN KEY (environment_id) REFERENCES environment (id)
);

CREATE TABLE quest_task(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    quest_id uuid NOT NULL,
    title varchar(64) NOT NULL,
    subtitle varchar(128) NOT NULL,
    description text,
    type varchar(32) NOT NULL,
    created_at timestamptz default NOW(),
    recurring boolean NOT NULL DEFAULT false,
    expires_at timestamptz,
    optional boolean NOT NULL DEFAULT false,
    CONSTRAINT quest_task_pk_1 PRIMARY KEY(id),
    CONSTRAINT quest_task_fk_1 FOREIGN KEY (quest_id) REFERENCES quest (id)
);

CREATE TABLE user_quest(
    quest_id uuid NOT NULL,
    user_id uuid NOT NULL,
    CONSTRAINT user_quest_pk_1 PRIMARY KEY (quest_id,user_id),
    CONSTRAINT user_quest_fk_1 FOREIGN KEY (quest_id) REFERENCES quest (id),
    CONSTRAINT user_quest_fk_2 FOREIGN KEY (user_id) REFERENCES app_user (id)
);

CREATE TABLE user_quest_task(
    quest_task_id uuid NOT NULL,
    user_id uuid NOT NULL,
    CONSTRAINT user_quest_task_pk_1 PRIMARY KEY (quest_task_id,user_id),
    CONSTRAINT user_quest_task_fk_1 FOREIGN KEY (quest_task_id) REFERENCES quest_task (id),
    CONSTRAINT user_quest_task_fk_2 FOREIGN KEY (user_id) REFERENCES app_user (id)
);
