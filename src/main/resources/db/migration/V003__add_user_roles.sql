CREATE TABLE user_role(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    user_id uuid NOT NULL,
    value varchar(16) NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    CONSTRAINT user_role_pk_1 PRIMARY KEY(id),
    CONSTRAINT user_role_uq_1 UNIQUE (user_id, value),
    CONSTRAINT user_role_fk_1 FOREIGN KEY (user_id) REFERENCES app_user (id),
    CONSTRAINT user_role_ch_1 check (value in ('USER', 'ADMIN'))
);
