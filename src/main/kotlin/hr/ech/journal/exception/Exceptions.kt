package hr.ech.journal.exception

class LoginFailedException : RuntimeException()

class AuthorizationFailedException : Throwable()

class ResourceNotFoundException : RuntimeException()
