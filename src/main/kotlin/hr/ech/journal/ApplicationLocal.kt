package hr.ech.journal

import hr.ech.journal.plugins.hikariDataSource

fun main() = runApp(
    hikariDataSource(
        url = "jdbc:postgresql://localhost:5432/journal?reWriteBatchedInserts=true",
        driverClassName = "org.postgresql.Driver",
        username = "journal-backend",
        password = "test-pass",
    ),
    jwtPrivateKey = "MIIBVgIBADANBgkqhkiG9w0BAQEFAASCAUAwggE8AgEAAkEAtC5fTCGuNPMMqhKtXqGhjEDFqestgjo+B8P1GBJLI24yCQjg+c2NnCJUFL78suxmPIEc93R4aB4sPb1GILQiUwIDAQABAkEAsZMmLnTfKKYE7DSeEq5+9Sf85rq1ziuyaJrSmZel39KXqEV/4At1/p+ZH4JuZZU53VJasqLI0FMgOvDKLyEdAQIhAO/ZJs9HziCGeiP04KX5uyNdGcpT/ramgX1RucOB3jSBAiEAwFCX6jx8Y05mgGOtoJ+Mx/o9YHORG3trFOEcJ6lw3NMCIFrMUD7kJgp2HkMCbxEmS0wF8L+BsMmgmyDAEZ0Lyv0BAiEApwOW2sqKr+d/ZxKpz+uf5G/y65eRtk85kJlqzRr+tBcCIQDONHcHbpDsEpg0Kkj9wZ6Agn9iugq8AypcFVDc/Z17+w==",
    jwtPublicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALQuX0whrjTzDKoSrV6hoYxAxanrLYI6PgfD9RgSSyNuMgkI4PnNjZwiVBS+/LLsZjyBHPd0eGgeLD29RiC0IlMCAwEAAQ==",
)
