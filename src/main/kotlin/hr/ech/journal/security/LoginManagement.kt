package hr.ech.journal.security

import at.favre.lib.crypto.bcrypt.BCrypt
import hr.ech.journal.database.JsonAggExpression
import hr.ech.journal.database.table.AppUserTable
import hr.ech.journal.database.table.UserRoleTable
import hr.ech.journal.exception.LoginFailedException
import hr.ech.journal.plugins.JWTGenerator
import org.postgresql.util.PGobject
import kotlinx.serialization.json.Json
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.UUID

fun login(
    username: String,
    password: String,
    jwtGenerator: JWTGenerator,
): String = transaction {
    val aggExpression = JsonAggExpression<List<String>>(listOf(UserRoleTable.value.name))
    AppUserTable
        .join(UserRoleTable, JoinType.INNER, UserRoleTable.userId, AppUserTable.id)
        .select(
            AppUserTable.id,
            AppUserTable.email,
            AppUserTable.passwordHash,
            aggExpression,
        ).where {
            (AppUserTable.email eq username)
                .and(AppUserTable.active eq true)
        }.groupBy(
            AppUserTable.id,
            AppUserTable.email,
            AppUserTable.passwordHash,
        ).map {
            UserWithRoles(
                it[AppUserTable.id].value,
                it[AppUserTable.email],
                it[AppUserTable.passwordHash],
                (it[aggExpression] as PGobject).toList(),
            )
        }.firstOrNull()
}?.takeIf { user ->
    isMatchingPassword(
        password = password,
        passwordHash = user.passwordHash,
    )
}?.let { user ->
    jwtGenerator.createJWTToken(
        email = username,
        userId = user.id.toString(),
        roles = user.roles,
    )
} ?: throw LoginFailedException()

fun isMatchingPassword(
    password: String,
    passwordHash: String,
) = BCrypt.verifyer().verify(password.toByteArray(), passwordHash.toByteArray()).verified

data class UserWithRoles(
    val id: UUID,
    val username: String,
    val passwordHash: String,
    val roles: List<String>,
)

fun PGobject.toList() = Json.Default.decodeFromString<List<String>>(this.value!!)
