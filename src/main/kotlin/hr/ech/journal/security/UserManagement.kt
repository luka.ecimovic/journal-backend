package hr.ech.journal.security

import hr.ech.journal.database.table.AppUserTable
import hr.ech.journal.database.table.UserRoleTable
import hr.ech.journal.plugins.encodePassword
import hr.ech.journal.routing.CreateEnvironmentDto
import hr.ech.journal.routing.RegisterResponseDto
import hr.ech.journal.service.createEnvironment
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.util.UUID

fun register(
    email: String,
    displayName: String,
    password: String,
) = transaction {
    AppUserTable.insertAndGetId {
        it[this.email] = email
        it[this.displayName] = displayName
        it[this.passwordHash] = encodePassword(password)
        it[this.active] = true // TODO remove
    }.let { userId ->
        createEnvironment(
            userId = userId.value,
            createEnvironmentDto = CreateEnvironmentDto(
                name = "$displayName's quests",
                description = "$displayName's default environment",
            ),
        )
        addRoleToUser(userId.value, Role.USER)
        RegisterResponseDto(userId.value)
    }
}

fun addRoleToUser(
    userId: UUID,
    role: Role,
) = UserRoleTable.insert {
    it[UserRoleTable.userId] = userId
    it[value] = role.name
}

fun setUserActiveStatus(
    currentUserId: UUID,
    currentUserRoles: List<Role>,
    targetUserId: UUID,
    active: Boolean,
) {
    transaction {
        if (currentUserRoles.contains(Role.ADMIN)) {
            AppUserTable.update({ AppUserTable.id eq targetUserId }) {
                it[this.active] = active
            }
            // TODO audit
        }
    }
}

fun grantRole(
    currentUserId: UUID,
    currentUserRoles: List<Role>,
    targetUserId: UUID,
    role: Role,
) {
    transaction {
        if (currentUserRoles.contains(Role.ADMIN)) {
            addRoleToUser(
                userId = targetUserId,
                role = role,
            )
            // TODO audit
        }
    }
}

enum class Role {
    USER,
    ADMIN,
}
