package hr.ech.journal

import hr.ech.journal.database.flywayMigrate
import hr.ech.journal.plugins.configureDI
import hr.ech.journal.plugins.configureDatabases
import hr.ech.journal.plugins.configureRouting
import hr.ech.journal.plugins.configureSecurity
import hr.ech.journal.plugins.configureSerialization
import hr.ech.journal.plugins.hikariDataSource
import hr.ech.journal.plugins.statusPages
import io.ktor.server.application.Application
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import javax.sql.DataSource

fun main() {
    runApp(
        hikariDataSource(
            url = System.getenv("JOURNAL_BACKEND_DB_URL"),
            // "org.postgresql.Driver",
            driverClassName = System.getenv("JOURNAL_BACKEND_DB_DRIVER"),
            username = System.getenv("JOURNAL_BACKEND_DB_USERNAME"),
            password = System.getenv("JOURNAL_BACKEND_DB_PASSWORD"),
        ),
        jwtPrivateKey = System.getenv("JOURNAL_BACKEND_JWT_PRIVATE_KEY"),
        jwtPublicKey = System.getenv("JOURNAL_BACKEND_JWT_PUBLIC_KEY"),
    )
}

fun runApp(
    dataSource: DataSource,
    jwtPrivateKey: String,
    jwtPublicKey: String,
) {
    flywayMigrate(dataSource)
    configureDatabases(dataSource)

    embeddedServer(
        factory = Netty,
        host = "0.0.0.0",
        port = 8080,
    ) {
        module(
            jwtPrivateKey = jwtPrivateKey,
            jwtPublicKey = jwtPublicKey,
        )
    }.start(wait = true)
}

fun Application.module(
    jwtPrivateKey: String,
    jwtPublicKey: String,
) {
    configureDI(jwtPrivateKey, jwtPublicKey)
    configureSecurity()
    configureSerialization()
    statusPages()
    configureRouting()
}
