package hr.ech.journal.routing

import hr.ech.journal.database.entity.User
import hr.ech.journal.exception.ResourceNotFoundException
import hr.ech.journal.plugins.InstantSerializer
import hr.ech.journal.plugins.getUserId
import hr.ech.journal.plugins.safeUuid
import hr.ech.journal.service.createEnvironment
import hr.ech.journal.service.getEnvironment
import hr.ech.journal.service.getEnvironments
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import kotlinx.serialization.Serializable
import java.time.Instant

fun Route.environmentRouting(routePath: String) {
    get(routePath) {
        call.respond(
            getEnvironments(call.getUserId()),
        )
    }

    get("$routePath/{environmentId}") {
        call.parameters["environmentId"]?.let { environmentId ->
            getEnvironment(
                userId = call.getUserId(),
                environmentId = environmentId.safeUuid(),
            )?.let {
                call.respond(it)
            } ?: throw ResourceNotFoundException()
        } ?: throw ResourceNotFoundException()
    }

    post(routePath) {
        createEnvironment(
            userId = call.getUserId(),
            createEnvironmentDto = call.receive(),
        )
        call.respond("")
    }
}

@Serializable
data class CreateEnvironmentDto(
    val name: String,
    val description: String,
)

@Serializable
data class GetEnvironmentDto(
    val id: String,
    val name: String,
    val description: String,
    @Serializable(with = InstantSerializer::class)
    val createdAt: Instant,
    val users: List<EnvironmentUserDto>,
)

@Serializable
data class EnvironmentUserDto(
    val username: String,
)

fun User.toEnvironmentUserDto() = EnvironmentUserDto(
    username = email,
)
