package hr.ech.journal.routing

import hr.ech.journal.exception.ResourceNotFoundException
import hr.ech.journal.plugins.getUserId
import hr.ech.journal.plugins.safeUuid
import hr.ech.journal.service.createQuest
import hr.ech.journal.service.getQuest
import hr.ech.journal.service.getQuests
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post

fun Route.questRouting(routePath: String) {
    get(routePath) {
        call.respond(
            getQuests(call.getUserId()),
        )
    }

    get("$routePath/{questId}") {
        call.parameters["questId"]?.let { questId ->
            getQuest(
                userId = call.getUserId(),
                questId = questId.safeUuid(),
            )?.let {
                call.respond(it)
            } ?: throw ResourceNotFoundException()
        } ?: throw ResourceNotFoundException()
    }

    post(routePath) {
        createQuest(
            userId = call.getUserId(),
            createQuestDto = call.receive(),
        )
        call.respond("")
    }
}
