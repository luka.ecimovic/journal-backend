package hr.ech.journal.routing

import hr.ech.journal.plugins.JWTGenerator
import hr.ech.journal.plugins.UUIDSerializer
import hr.ech.journal.plugins.getUserId
import hr.ech.journal.plugins.getUserRoles
import hr.ech.journal.plugins.safeUuid
import hr.ech.journal.security.Role
import hr.ech.journal.security.grantRole
import hr.ech.journal.security.login
import hr.ech.journal.security.register
import hr.ech.journal.security.setUserActiveStatus
import hr.ech.journal.service.usernameExists
import hr.ech.journal.service.validaPassword
import io.ktor.server.application.call
import io.ktor.server.plugins.BadRequestException
import io.ktor.server.plugins.NotFoundException
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.head
import io.ktor.server.routing.patch
import io.ktor.server.routing.post
import kotlinx.serialization.Serializable
import java.util.UUID

fun Route.userRouting(jwtGenerator: JWTGenerator) {
    post("/api/login") {
        call.respond(
            call.receive<CredentialsDto>().let {
                TokenDto(
                    login(
                        it.username,
                        it.password,
                        jwtGenerator,
                    ),
                )
            },
        )
    }

    post("/api/register") {
        call.respond(
            call.receive<RegisterDto>().let { registerDto ->
                validaPassword(registerDto.password)?.let { password ->
                    register(
                        email = registerDto.username,
                        displayName = registerDto.displayName,
                        password = password,
                    )
                } ?: throw BadRequestException("PASSWORD_TOO_WEAK")
            },
        )
    }

    patch("/api/users/{userId}/active") {
        call.parameters["userId"]?.let { userId ->
            setUserActiveStatus(
                currentUserId = call.getUserId(),
                currentUserRoles = call.getUserRoles(),
                targetUserId = userId.safeUuid(),
                call.receive<ActiveDto>().active,
            )
            call.respond("")
        } ?: throw NotFoundException()
    }

    post("/api/users/{userId}/role") {
        call.parameters["userId"]?.let { userId ->
            grantRole(
                currentUserId = call.getUserId(),
                currentUserRoles = call.getUserRoles(),
                targetUserId = userId.safeUuid(),
                call.receive<AddRoleDto>().role.toRole(),
            )
            call.respond("")
        } ?: throw NotFoundException()
    }

    head("/api/users/exists") {
        call.request.queryParameters["username"]?.let { username ->
            if (usernameExists(username = username)) {
                call.respond(Unit)
            } else {
                throw NotFoundException()
            }
        } ?: throw BadRequestException("")
    }
}

@Serializable
data class CredentialsDto(
    val username: String,
    val password: String,
)

@Serializable
data class RegisterDto(
    val username: String,
    val displayName: String,
    val password: String,
)

@Serializable
data class TokenDto(
    val token: String,
)

@Serializable
data class ActiveDto(
    val active: Boolean,
)

@Serializable
data class AddRoleDto(
    val role: String,
)

@Serializable
data class RegisterResponseDto(
    @Serializable(with = UUIDSerializer::class)
    val id: UUID,
)

fun String.toRole(): Role = Role.valueOf(this)
