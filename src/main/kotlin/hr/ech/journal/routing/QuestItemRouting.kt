package hr.ech.journal.routing

import io.ktor.server.routing.Route
import io.ktor.server.routing.post

fun Route.questItemRouting(routeUrl: String) {
    fun createQuestItem() = post(routeUrl) {}
}
