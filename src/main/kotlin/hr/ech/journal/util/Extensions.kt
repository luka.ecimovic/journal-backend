package hr.ech.journal.util

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

const val DEFAULT_PAGE_SIZE = 20

fun String.toLocalDateTime(): LocalDateTime = LocalDateTime.parse(this)

fun LocalDateTime.toEpochMilli(zoneOffset: ZoneOffset) = toInstant(zoneOffset)?.toEpochMilli()

fun epochMillisFromDateTimeString(string: String?) = string?.toLocalDateTime()?.toEpochMilli(ZoneOffset.UTC)

fun Instant.toIsoDateTime(): String = DateTimeFormatter.ISO_DATE_TIME.format(this)

fun Boolean.toInt() = if (this) 1 else 0

fun Any?.isNotNull() = this != null
