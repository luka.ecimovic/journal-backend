package hr.ech.journal.util

import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.call
import io.ktor.util.pipeline.PipelineContext

fun PipelineContext<Unit, ApplicationCall>.safePage() = safeInt("page", 1)

fun PipelineContext<Unit, ApplicationCall>.safeLimit() = safeInt("limit", DEFAULT_PAGE_SIZE)

fun PipelineContext<Unit, ApplicationCall>.safeInt(
    paramName: String,
    defaultValue: Int,
) = safeParam(
    paramName = paramName,
    defaultValue = defaultValue,
) {
    it?.toIntOrNull()
}

fun <T> PipelineContext<Unit, ApplicationCall>.safeParam(
    paramName: String,
    defaultValue: T,
    converter: (String?) -> T?,
) = converter(call.request.queryParameters[paramName]) ?: defaultValue
