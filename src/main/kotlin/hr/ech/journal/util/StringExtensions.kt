package hr.ech.journal.util

import java.util.UUID

fun String.toUuid(): UUID? = runCatching { UUID.fromString(this) }.getOrNull()
