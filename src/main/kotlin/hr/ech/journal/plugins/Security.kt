package hr.ech.journal.plugins

import at.favre.lib.crypto.bcrypt.BCrypt
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.RSAKeyProvider
import io.ktor.server.application.Application
import io.ktor.server.auth.authentication
import io.ktor.server.auth.jwt.JWTPrincipal
import io.ktor.server.auth.jwt.jwt
import org.koin.ktor.ext.inject
import java.security.KeyFactory
import java.security.PrivateKey
import java.security.PublicKey
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.Base64

fun Application.configureSecurity(
    jwtAudience: String = "test",
    jwtDomain: String = "localhost",
    jwtRealm: String = "journal",
) {
    val algorithm by inject<Algorithm>()
    authentication {
        jwt {
            realm = jwtRealm
            verifier(
                JWT
                    .require(algorithm)
                    .withAudience(jwtAudience)
                    .withIssuer(jwtDomain)
                    .build(),
            )
            validate { credential ->
                if (credential.payload.audience.contains(jwtAudience)) JWTPrincipal(credential.payload) else null
            }
        }
    }
}

class JWTGenerator(
    private val jwtAudience: String = "test",
    private val jwtDomain: String = "localhost",
    private val algorithm: Algorithm,
) {
    fun createJWTToken(
        email: String,
        userId: String,
        roles: List<String>,
    ): String =
        JWT.create().apply {
            withSubject(email)
            withAudience(jwtAudience)
            withIssuer(jwtDomain)
            withClaim("userId", userId)
            withClaim("roles", roles)
        }.sign(algorithm)
}

class JWTKeyProvider(
    jwtPrivateKey: String,
    jwtPublicKey: String,
) : RSAKeyProvider {
    init {
        loadKeys(
            publicKey = jwtPublicKey,
            privateKey = jwtPrivateKey,
        )
    }

    private lateinit var publicKey: PublicKey
    private lateinit var privateKey: PrivateKey

    override fun getPublicKeyById(keyId: String?) = publicKey as RSAPublicKey

    override fun getPrivateKey() = privateKey as RSAPrivateKey

    override fun getPrivateKeyId() = "" // TODO

    private fun loadKeys(
        publicKey: String,
        privateKey: String,
    ) {
        loadPrivateKey(privateKey)
        loadPublicKey(publicKey)
    }

    private fun loadPublicKey(publicKeyString: String) {
        val keyBytes: ByteArray = Base64.getDecoder().decode(publicKeyString)
        val keySpec = X509EncodedKeySpec(keyBytes)
        val keyFactory: KeyFactory = KeyFactory.getInstance("RSA")
        publicKey = keyFactory.generatePublic(keySpec)
    }

    private fun loadPrivateKey(privateKeyString: String) {
        val keyBytes: ByteArray = Base64.getDecoder().decode(privateKeyString)
        val keyFactory: KeyFactory = KeyFactory.getInstance("RSA")
        val keySpec = PKCS8EncodedKeySpec(keyBytes)
        privateKey = keyFactory.generatePrivate(keySpec)
    }
}

const val BCRYPT_COST = 12
fun encodePassword(password: String): String = BCrypt.withDefaults().hashToString(BCRYPT_COST, password.toCharArray())
