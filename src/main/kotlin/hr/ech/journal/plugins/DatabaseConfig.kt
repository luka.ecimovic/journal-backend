package hr.ech.journal.plugins

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import javax.sql.DataSource

fun configureDatabases(dataSource: DataSource) = Database.connect(dataSource)

fun hikariDataSource(
    url: String,
    driverClassName: String,
    username: String = "",
    password: String = "",
) = HikariDataSource(
    HikariConfig().apply {
        this.jdbcUrl = url
        this.username = username
        this.password = password
        this.driverClassName = driverClassName
    },
)
// config.addDataSourceProperty("cachePrepStmts", "true");
// config.addDataSourceProperty("prepStmtCacheSize", "250");
// config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
