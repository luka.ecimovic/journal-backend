package hr.ech.journal.plugins

import hr.ech.journal.exception.AuthorizationFailedException
import hr.ech.journal.exception.LoginFailedException
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.install
import io.ktor.server.plugins.BadRequestException
import io.ktor.server.plugins.NotFoundException
import io.ktor.server.plugins.statuspages.StatusPages
import io.ktor.server.response.respond
import io.ktor.server.response.respondText

fun Application.statusPages() {
    install(StatusPages) {
        exception<Throwable> { call, cause ->
            cause.printStackTrace()
            call.respondText(text = "500: $cause", status = HttpStatusCode.InternalServerError)
        }
        exception<NotFoundException> { call, cause ->
            call.respond(status = HttpStatusCode.NotFound, message = cause.message ?: "Not found")
        }
        exception<BadRequestException> { call, cause ->
            cause.printStackTrace()
            call.respond(status = HttpStatusCode.BadRequest, message = cause.message ?: "Bad request")
        }
        exception<AuthorizationFailedException> { call, cause ->
            call.respond(status = HttpStatusCode.Unauthorized, message = cause.message ?: "Authentication failed")
        }
        exception<LoginFailedException> { call, cause ->
            call.respond(status = HttpStatusCode.Unauthorized, message = cause.message ?: "Login failed")
        }
    }
}
