package hr.ech.journal.plugins

import com.auth0.jwt.algorithms.Algorithm
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.server.application.Application
import io.ktor.server.application.install
import io.ktor.server.application.log
import io.ktor.util.logging.Logger
import org.koin.dsl.module
import org.koin.ktor.plugin.Koin
import org.koin.logger.slf4jLogger
import redis.clients.jedis.JedisPooled

fun Application.configureDI(
    jwtPrivateKey: String,
    jwtPublicKey: String,
) {
    install(Koin) {
        slf4jLogger()
        modules(
            appModule(log),
            securityModule(jwtPrivateKey, jwtPublicKey),
        )
    }
}

fun appModule(logger: Logger) = module {
    single { JedisPooled("http://127.0.0.1:6379") }
    single(createdAtStart = true) { HttpClient(CIO) }
    single { logger }
}

fun securityModule(
    jwtPrivateKey: String,
    jwtPublicKey: String,
) = module {
    single(createdAtStart = true) { JWTKeyProvider(jwtPrivateKey, jwtPublicKey) }
    single { JWTGenerator(algorithm = get()) }
    single(createdAtStart = true) { Algorithm.RSA256(get<JWTKeyProvider>()) }
}
