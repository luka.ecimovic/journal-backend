package hr.ech.journal.plugins

import hr.ech.journal.exception.AuthorizationFailedException
import hr.ech.journal.routing.environmentRouting
import hr.ech.journal.routing.questItemRouting
import hr.ech.journal.routing.questRouting
import hr.ech.journal.routing.userRouting
import hr.ech.journal.security.Role
import hr.ech.journal.util.toUuid
import io.ktor.server.application.Application
import io.ktor.server.application.ApplicationCall
import io.ktor.server.auth.authenticate
import io.ktor.server.auth.authentication
import io.ktor.server.auth.jwt.JWTPrincipal
import io.ktor.server.http.content.staticResources
import io.ktor.server.plugins.BadRequestException
import io.ktor.server.routing.Route
import io.ktor.server.routing.Routing
import io.ktor.server.routing.routing
import org.koin.ktor.ext.inject
import java.util.UUID

fun Application.configureRouting() {
    val jwtGenerator by inject<JWTGenerator>()

    routing {
        staticResources("/static", "static")
        authenticate {
            authenticatedRoutes()
        }
        openRoutes(jwtGenerator)
    }
}

fun Routing.openRoutes(jwtGenerator: JWTGenerator) {
    userRouting(jwtGenerator)
}

fun Route.authenticatedRoutes() {
    questRouting("/api/quests")
    questItemRouting("/api/quests/items")
    environmentRouting("/api/environments")
}

fun ApplicationCall.getUserId(): UUID =
    authentication.principal<JWTPrincipal>()?.get("userId")?.toUuid()
        ?: throw AuthorizationFailedException()

fun String.safeUuid(): UUID = toUuid()
    ?: throw BadRequestException("INVALID_UUID_PARAMETER")

fun ApplicationCall.getUserRoles(): List<Role> =
    authentication.principal<JWTPrincipal>()?.getListClaim("roles", String::class)?.map { Role.valueOf(it) }
        ?: throw AuthorizationFailedException()
