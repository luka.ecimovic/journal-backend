package hr.ech.journal.database

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ColumnType
import org.jetbrains.exposed.sql.Table
import java.util.UUID

fun Table.uuid(
    name: String,
    columnType: ColumnType,
): Column<UUID> = registerColumn(name, PostgresqlUuidColumnType(columnType))

class PostgresqlUuidColumnType(private val type: ColumnType) : ColumnType() {
    override fun sqlType(): String = type.sqlType()

    override fun valueToDB(value: Any?): Any? {
        return when (value) {
            is UUID -> super.valueToDB(value.toString())
            else -> super.valueToDB(value)
        }
    }

    override fun valueFromDB(value: Any): Any {
        return UUID.fromString(value.toString())
    }

    override fun notNullValueToDB(value: Any) = when (value) {
        is UUID -> super.notNullValueToDB(value.toString())
        else -> super.notNullValueToDB(value)
    }
}
