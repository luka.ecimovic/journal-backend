package hr.ech.journal.database

import org.jetbrains.exposed.sql.Table

const val BCRYPT_HASH_LENGTH = 72

fun Table.bcryptHash(name: String) = varchar(name, BCRYPT_HASH_LENGTH)
