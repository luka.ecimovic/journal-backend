package hr.ech.journal.database.entity

import hr.ech.journal.database.table.QuestTaskTable
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import java.util.UUID

class QuestTask(id: EntityID<UUID>) : Entity<UUID>(id) {
    companion object : EntityClass<UUID, QuestTask>(QuestTaskTable)

    var title by QuestTaskTable.title
    var subtitle by QuestTaskTable.subtitle
    var description by QuestTaskTable.description
    var createdAt by QuestTaskTable.createdAt
    var type by QuestTaskTable.type
    var recurring by QuestTaskTable.recurring
    var expiresAt by QuestTaskTable.expiresAt
    var optional by QuestTaskTable.optional
}
