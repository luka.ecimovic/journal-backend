package hr.ech.journal.database.entity

import hr.ech.journal.database.table.EnvironmentTable
import hr.ech.journal.database.table.EnvironmentUserTable
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.SizedIterable
import java.util.UUID

class Environment(id: EntityID<UUID>) : Entity<UUID>(id) {
    companion object : EntityClass<UUID, Environment>(EnvironmentTable)

    var name by EnvironmentTable.name
    var description by EnvironmentTable.description
    var createdAt by EnvironmentTable.createdAt

    val environmentUsers: SizedIterable<User> by User via EnvironmentUserTable
}
