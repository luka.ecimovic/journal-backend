package hr.ech.journal.database.entity

import hr.ech.journal.database.table.AppUserTable
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import java.util.UUID

class User(id: EntityID<UUID>) : Entity<UUID>(id) {
    companion object : EntityClass<UUID, User>(AppUserTable)

    var displayName by AppUserTable.displayName
    var email by AppUserTable.email
    var active by AppUserTable.active
    var passwordHash by AppUserTable.passwordHash
}
