package hr.ech.journal.database.entity

import hr.ech.journal.database.table.AppUserTable
import hr.ech.journal.database.table.UserRoleTable
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.SizedIterable
import java.util.UUID

class UserWithRoles(id: EntityID<UUID>) : Entity<UUID>(id) {
    companion object : EntityClass<UUID, UserWithRoles>(AppUserTable)

    var displayName by AppUserTable.displayName
    var email by AppUserTable.email
    var active by AppUserTable.active
    var passwordHash by AppUserTable.passwordHash

    val userRoles: SizedIterable<UserRole> by UserRole referrersOn UserRoleTable.userId
}
