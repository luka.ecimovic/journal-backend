package hr.ech.journal.database.entity

import hr.ech.journal.database.table.UserRoleTable
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import java.util.UUID

class UserRole(id: EntityID<UUID>) : Entity<UUID>(id) {
    companion object : EntityClass<UUID, UserRole>(UserRoleTable)
    var value by UserRoleTable.value
}
