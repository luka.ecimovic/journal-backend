package hr.ech.journal.database.entity

import hr.ech.journal.database.table.QuestTable
import hr.ech.journal.database.table.QuestTaskTable
import hr.ech.journal.database.table.UserQuestTable
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.SizedIterable
import java.util.UUID

class Quest(id: EntityID<UUID>) : Entity<UUID>(id) {
    companion object : EntityClass<UUID, Quest>(QuestTable)

    var title by QuestTable.title
    var subtitle by QuestTable.subtitle
    var description by QuestTable.description
    var createdAt by QuestTable.createdAt
    var recurring by QuestTable.recurring
    var expiresAt by QuestTable.expiresAt
    var environment by Environment referencedOn QuestTable.environmentId
    var createdBy by User referencedOn QuestTable.createdBy

    val questTasks: SizedIterable<QuestTask> by QuestTask referrersOn QuestTaskTable.questId

    val assignedUsers: SizedIterable<User> by User via UserQuestTable
}
