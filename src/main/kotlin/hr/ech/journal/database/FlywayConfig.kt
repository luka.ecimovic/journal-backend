package hr.ech.journal.database

import org.flywaydb.core.Flyway
import javax.sql.DataSource

fun flywayMigrate(dataSource: DataSource) = Flyway.configure().dataSource(dataSource).load()?.migrate()
