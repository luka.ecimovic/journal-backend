package hr.ech.journal.database

import org.jetbrains.exposed.sql.Expression
import org.jetbrains.exposed.sql.QueryBuilder

class JsonAggExpression<T>(
    private val  fields: List<String>,
) : Expression<T>() {
    override fun toQueryBuilder(queryBuilder: QueryBuilder) {
        queryBuilder.append("json_agg(${joinFields()})")
    }

    private fun joinFields() = fields.joinToString(", ")
}
