package hr.ech.journal.database.table

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table
import java.util.UUID

object UserQuestTable : Table("user_quest") {
    val userId: Column<EntityID<UUID>> = reference(
        name = "user_id",
        refColumn = AppUserTable.id,
    )

    val questId: Column<EntityID<UUID>> = reference(
        name = "quest_id",
        refColumn = QuestTable.id,
    )

    override val primaryKey: PrimaryKey = PrimaryKey(userId, questId)
}
