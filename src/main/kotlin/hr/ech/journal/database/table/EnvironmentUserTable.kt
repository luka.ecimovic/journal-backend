package hr.ech.journal.database.table

import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.json.json

object EnvironmentUserTable : Table("environment_user") {
    val environmentId = reference(
        name = "environment_id",
        refColumn = EnvironmentTable.id,
    )

    val userId = reference(
        name = "user_id",
        refColumn = AppUserTable.id,
    )

    val permissions = json(
        name = "permissions",
        serialize = { Json.Default.encodeToString(it) },
        deserialize = { Json.decodeFromString(it) as EnvironmentPermissions },
    )

    override val primaryKey: PrimaryKey = PrimaryKey(environmentId, userId)
}

@Serializable
data class EnvironmentPermissions(
    val permissions: List<String>,
)
