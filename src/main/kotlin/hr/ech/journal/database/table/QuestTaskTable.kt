package hr.ech.journal.database.table

import hr.ech.journal.database.timestampWithTimeZone
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column
import java.time.Instant
import java.util.UUID

const val QUEST_ITEM_TITLE_LENGTH = 64
const val QUEST_ITEM_SUBTITLE_LENGTH = 128
const val QUEST_ITEM_TYPE_LENGTH = 32

object QuestTaskTable : IdTable<UUID>("quest_task") {
    override val id: Column<EntityID<UUID>> = uuid("id").entityId()

    val questId: Column<EntityID<UUID>> = reference("quest_id", QuestTable.id)

    val title: Column<String> = varchar("title", QUEST_ITEM_TITLE_LENGTH)

    val subtitle: Column<String> = varchar("subtitle", QUEST_ITEM_SUBTITLE_LENGTH)

    val description: Column<String> = text("description")

    val type: Column<String> = varchar("type", QUEST_ITEM_TYPE_LENGTH)

    val createdAt: Column<Instant> = timestampWithTimeZone("created_at")

    val recurring: Column<Boolean> = bool("recurring")

    val expiresAt: Column<Instant?> = timestampWithTimeZone("expires_at").nullable()

    val optional: Column<Boolean> = bool("optional")

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}
