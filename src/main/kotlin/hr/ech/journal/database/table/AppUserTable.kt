package hr.ech.journal.database.table

import hr.ech.journal.database.bcryptHash
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column
import java.util.UUID

const val DISPLAY_NAME_LENGTH = 64
const val EMAIL_LENGTH = 128

object AppUserTable : IdTable<UUID>("app_user") {
    override val id: Column<EntityID<UUID>> = uuid("id").entityId()

    val displayName: Column<String> = varchar(
        "display_name",
        DISPLAY_NAME_LENGTH,
    )

    val email: Column<String> = varchar("email", EMAIL_LENGTH)

    val passwordHash: Column<String> = bcryptHash("password_hash")

    val active: Column<Boolean> = bool("active")

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}
