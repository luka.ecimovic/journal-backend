package hr.ech.journal.database.table

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table
import java.util.UUID

object UserQuestItemTable : Table("user_quest_item") {
    val userId: Column<EntityID<UUID>> = reference(
        name = "user_id",
        refColumn = AppUserTable.id,
    )

    val questItemId: Column<EntityID<UUID>> = reference(
        name = "quest_item_id",
        refColumn = QuestTaskTable.id,
    )

    override val primaryKey: PrimaryKey = PrimaryKey(userId, questItemId)
}
