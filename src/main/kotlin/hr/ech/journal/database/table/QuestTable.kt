package hr.ech.journal.database.table

import hr.ech.journal.database.timestampWithTimeZone
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column
import java.time.Instant
import java.util.UUID

const val QUEST_TITLE_LENGTH = 64
const val QUEST_SUBTITLE_LENGTH = 128

object QuestTable : IdTable<UUID>("quest") {
    override val id: Column<EntityID<UUID>> = uuid("id").entityId()

    val title: Column<String> = varchar("title", QUEST_TITLE_LENGTH)

    val subtitle: Column<String> = varchar("subtitle", QUEST_SUBTITLE_LENGTH)

    val description: Column<String> = text("description")

    val createdAt: Column<Instant> = timestampWithTimeZone("created_at")

    val recurring: Column<Boolean> = bool("recurring")

    val expiresAt: Column<Instant?> = timestampWithTimeZone("expires_at").nullable()

    val createdBy: Column<EntityID<UUID>> = reference(
        name = "created_by",
        refColumn = AppUserTable.id,
    )

    val environmentId: Column<EntityID<UUID>> = reference(
        name = "environment_id",
        refColumn = EnvironmentTable.id,
    )

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}
