package hr.ech.journal.database.table

import hr.ech.journal.database.timestampWithTimeZone
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column
import java.time.Instant
import java.util.UUID

const val QUEST_ITEM_STATUS_LENGTH = 32

object QuestItemStatusTable : IdTable<UUID>("quest_item_status") {
    override val id: Column<EntityID<UUID>> = uuid("id").entityId()

    val questItemId: Column<EntityID<UUID>> = reference(
        name = "quest_item_id",
        refColumn = QuestTaskTable.id,
    )

    val status: Column<String> = varchar("status", QUEST_ITEM_STATUS_LENGTH)

    val timestamp: Column<Instant> = timestampWithTimeZone("timestamp")

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}
