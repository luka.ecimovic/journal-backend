package hr.ech.journal.database.table

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column
import java.util.UUID

const val ROLE_CODE_LENGTH = 16

object UserRoleTable : IdTable<UUID>("user_role") {
    override val id: Column<EntityID<UUID>> = uuid("id").entityId()

    val userId = reference(
        name = "user_id",
        refColumn = AppUserTable.id,
    )

    val value: Column<String> = varchar(
        name = "value",
        length = ROLE_CODE_LENGTH,
    )

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}
