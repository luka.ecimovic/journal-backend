package hr.ech.journal.database.table

import hr.ech.journal.database.timestampWithTimeZone
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column
import java.time.Instant
import java.util.UUID

const val ENVIRONMENT_NAME_LENGTH = 64
const val ENVIRONMENT_DESCRIPTION_LENGTH = 128

object EnvironmentTable : IdTable<UUID>("environment") {
    override val id: Column<EntityID<UUID>> = uuid("id").entityId()

    val name: Column<String> = varchar(
        name = "name",
        length = ENVIRONMENT_NAME_LENGTH,
    )

    val description: Column<String> = varchar(
        name = "description",
        length = ENVIRONMENT_DESCRIPTION_LENGTH,
    )

    val createdAt: Column<Instant> = timestampWithTimeZone("created_at")

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}
