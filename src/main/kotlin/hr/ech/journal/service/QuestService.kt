package hr.ech.journal.service

import hr.ech.journal.database.entity.Quest
import hr.ech.journal.database.entity.QuestTask
import hr.ech.journal.database.table.AppUserTable
import hr.ech.journal.database.table.QuestTable
import hr.ech.journal.database.table.QuestTaskTable
import hr.ech.journal.database.table.UserQuestTable
import hr.ech.journal.plugins.InstantSerializer
import hr.ech.journal.plugins.UUIDSerializer
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant
import java.util.UUID

fun getQuests(userId: UUID) = transaction {
    questUserJoin().selectAll().where {
        (QuestTable.createdBy eq userId) or (UserQuestTable.userId eq userId)
    }.map {
        kotlin.runCatching { QuestTask.wrapRow(it) }
        Quest.wrapRow(it)
    }.map { it.toDto() }
}

fun getQuest(
    userId: UUID,
    questId: UUID,
) = transaction {
    questUserJoin().selectAll().where {
        (QuestTable.id eq questId) and (
                (QuestTable.createdBy eq userId) or (UserQuestTable.userId eq userId)
                )
    }.limit(1).map {
        QuestTask.wrapRow(it)
        Quest.wrapRow(it)
    }.map { it.toDto() }.firstOrNull()
}

fun questUserJoin() = QuestTable.join(
    joinType = JoinType.LEFT,
    otherTable = QuestTaskTable,
    onColumn = QuestTable.id,
    otherColumn = QuestTaskTable.questId,
).join(
    joinType = JoinType.INNER,
    otherTable = UserQuestTable,
    onColumn = QuestTable.id,
    otherColumn = UserQuestTable.questId,
)

fun createQuest(
    userId: UUID,
    createQuestDto: CreateQuestDto,
) {
    transaction {
        val questId = QuestTable.insertAndGetId {
            it[title] = createQuestDto.title
            it[subtitle] = createQuestDto.subtitle
            it[description] = createQuestDto.description ?: ""
            it[createdBy] = userId
            it[recurring] = createQuestDto.recurring
            it[expiresAt] = createQuestDto.expiresAt
            it[environmentId] = createQuestDto.environmentId
        }.value
        if (createQuestDto.autoAssign) {
            UserQuestTable.insert {
                it[UserQuestTable.userId] = userId
                it[UserQuestTable.questId] = questId
            }
        }
    }
}

fun assignQuest(
    currentUserId: String,
    assigneeEmail: String,
    questId: UUID,
) = transaction {
    val userId = AppUserTable.selectAll().where { AppUserTable.email eq assigneeEmail }.first()[AppUserTable.id].value
    UserQuestTable.insert {
        it[UserQuestTable.userId] = userId
        it[UserQuestTable.questId] = questId
    }
    // TODO audit
}

@Serializable
data class QuestDto(
    val id: String,
    val title: String,
    val shortDescription: String?,
    val longDescription: String?,
    @Serializable(with = InstantSerializer::class)
    val createdAt: Instant,
    val recurring: Boolean,
    @Serializable(with = InstantSerializer::class)
    val expiresAt: Instant?,
    val environmentId: String,
    val questItems: List<QuestTaskDto>,
)

@Serializable
data class QuestTaskDto(
    val id: String,
    val title: String,
    val subtitle: String,
    val longDescription: String? = null,
    val type: String,
    @Serializable(with = InstantSerializer::class)
    val createdAt: Instant,
    val recurring: Boolean,
    @Serializable(with = InstantSerializer::class)
    val expiresAt: Instant?,
)

@Serializable
data class CreateQuestDto(
    val title: String,
    val subtitle: String,
    val description: String? = null,
    val recurring: Boolean,
    @Serializable(with = InstantSerializer::class)
    val expiresAt: Instant? = null,
    @Serializable(with = UUIDSerializer::class)
    val environmentId: UUID,
    val autoAssign: Boolean = true,
)

fun Quest.toDto() = QuestDto(
    id = id.value.toString(),
    title = title,
    shortDescription = subtitle,
    longDescription = description,
    createdAt = createdAt,
    recurring = recurring,
    expiresAt = expiresAt,
    environmentId = environment.id.value.toString(),
    questItems = questTasks.map { questItem ->
        QuestTaskDto(
            id = questItem.id.value.toString(),
            title = questItem.title,
            subtitle = questItem.subtitle,
            longDescription = questItem.description,
            type = questItem.type,
            createdAt = questItem.createdAt,
            recurring = questItem.recurring,
            expiresAt = questItem.expiresAt,
        )
    },
)
