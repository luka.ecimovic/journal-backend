package hr.ech.journal.service

import hr.ech.journal.database.table.AppUserTable
import hr.ech.journal.util.isNotNull
import org.jetbrains.exposed.sql.intLiteral
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.UUID

fun userIdExists(userId: UUID) = transaction {
    AppUserTable.select(intLiteral(1)).where {
        AppUserTable.id eq userId
    }.limit(1)
}.firstOrNull() != null

fun usernameExists(username: String) = findUserByUsername(username).isNotNull()

fun findUserByUsername(username: String) = transaction {
    AppUserTable.select(intLiteral(1))
        .where {
            AppUserTable.email eq username
        }
        .limit(1)
        .firstOrNull()
}
