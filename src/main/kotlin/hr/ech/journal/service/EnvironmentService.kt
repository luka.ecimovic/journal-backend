package hr.ech.journal.service

import hr.ech.journal.database.entity.Environment
import hr.ech.journal.database.entity.User
import hr.ech.journal.database.table.AppUserTable
import hr.ech.journal.database.table.EnvironmentPermissions
import hr.ech.journal.database.table.EnvironmentTable
import hr.ech.journal.database.table.EnvironmentUserTable
import hr.ech.journal.routing.CreateEnvironmentDto
import hr.ech.journal.routing.GetEnvironmentDto
import hr.ech.journal.routing.toEnvironmentUserDto
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.UUID

fun getEnvironments(userId: UUID) = transaction {
    EnvironmentTable.join(
        joinType = JoinType.INNER,
        otherTable = EnvironmentUserTable,
        onColumn = EnvironmentTable.id,
        otherColumn = EnvironmentUserTable.environmentId,
    ).join(
        joinType = JoinType.INNER,
        otherTable = AppUserTable,
        onColumn = EnvironmentUserTable.userId,
        otherColumn = AppUserTable.id,
    ).selectAll().where { (EnvironmentUserTable.userId eq userId) }.map {
        User.wrapRow(it)
        Environment.wrapRow(it)
    }.map {
        it.toDto()
    }
}

fun getEnvironment(
    userId: UUID,
    environmentId: UUID,
): GetEnvironmentDto? = transaction {
    Environment.find {
        (EnvironmentTable.id eq environmentId) and (EnvironmentUserTable.userId eq userId)
    }.firstOrNull()?.toDto()
}

fun createEnvironment(
    userId: UUID,
    createEnvironmentDto: CreateEnvironmentDto,
) = transaction {
    EnvironmentTable.insertAndGetId {
        it[name] = createEnvironmentDto.name
        it[description] = createEnvironmentDto.description
    }.value.let { environmentId ->
        EnvironmentUserTable.insert(
            userId = userId,
            environmentId = environmentId,
        )
    }
}

fun addUserToEnvironment(
    currentUserId: UUID,
    environmentId: UUID,
    newUserId: UUID,
) = transaction {
    if (EnvironmentUserTable.hasPermission(
            userId = currentUserId,
            environmentId = environmentId,
            Permission.ADD_USER,
        )
    ) {
        EnvironmentUserTable.insert(
            userId = newUserId,
            environmentId = environmentId,
        )
    }
}

fun EnvironmentUserTable.insert(
    userId: UUID,
    environmentId: UUID,
    permissions: List<String> = Permission.entries.toList().map { it.name },
) = insert {
    it[EnvironmentUserTable.userId] = userId
    it[EnvironmentUserTable.environmentId] = environmentId
    it[EnvironmentUserTable.permissions] = EnvironmentPermissions(permissions)
}

fun EnvironmentUserTable.hasPermission(
    userId: UUID,
    environmentId: UUID,
    permission: Permission,
) = selectAll().where {
    (EnvironmentUserTable.userId eq userId) and (EnvironmentUserTable.environmentId eq environmentId)
}.firstOrNull()?.let {
    it[permissions].permissions.contains(permission.name)
} ?: false

fun Environment.toDto() = GetEnvironmentDto(
    id = id.value.toString(),
    name = name,
    description = description,
    createdAt = createdAt,
    users = environmentUsers.map { user ->
        user.toEnvironmentUserDto()
    },
)

enum class Permission {
    VIEW,
    EDIT,
    DELETE,
    ADD_USER,
}
