package hr.ech.journal.service

import org.passay.CharacterRule
import org.passay.EnglishCharacterData
import org.passay.LengthRule
import org.passay.PasswordData
import org.passay.PasswordValidator
import org.passay.RuleResult
import org.passay.WhitespaceRule

const val MINIMUM_PASSWORD_LENGTH = 8
const val MAXIMUM_PASSWORD_LENGTH = 20

fun validaPassword(password: String) = validatePasswordStrength(password).let { ruleResult ->
    password.takeIf { ruleResult.isValid }
}

fun validatePasswordStrength(password: String): RuleResult = PasswordValidator(
    listOf(
        LengthRule(MINIMUM_PASSWORD_LENGTH, MAXIMUM_PASSWORD_LENGTH),
        WhitespaceRule(),
        CharacterRule(EnglishCharacterData.UpperCase, 1),
        CharacterRule(EnglishCharacterData.LowerCase, 1),
        CharacterRule(EnglishCharacterData.Digit, 1),
        CharacterRule(EnglishCharacterData.Special, 1),
    ),
).validate(PasswordData(password))
